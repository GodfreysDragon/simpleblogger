# Simple Blog App (Testing)

An Angular-based one-page blogging application, that provides both a list of all posts in blog and individual views of each post. Currently in development for further additions.

To run: navigate to App/ via command line, enter 'node ../scripts/web-server.js', and then navigate to localhost:8000 in your given browser.

Requirements: Node.

_Uses the Angular Seed application skeleton (available at https://github.com/angular/angular-seed/tree/69c9416c8407fd5806aab3c63916dfcf0522ecbc)._