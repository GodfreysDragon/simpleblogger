'use strict'

angular.module('spBlogger.posts.services',[]);

angular.module('spBlogger.posts.services', []).factory('postService', function() {
  return {
    posts: [{
      id: 1,
      title: 'Title 1',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Metus vulputate eu scelerisque felis imperdiet proin fermentum leo. Nam aliquam sem et tortor consequat id porta. Vitae sapien pellentesque habitant morbi tristique senectus et netus et. Tempor commodo ullamcorper a lacus vestibulum sed. Iaculis eu non diam phasellus. Morbi tincidunt augue interdum velit euismod in pellentesque massa. Quis risus sed vulputate odio ut enim. Magnis dis parturient montes nascetur ridiculus mus mauris vitae. Convallis tellus id interdum velit laoreet. Arcu vitae elementum curabitur vitae nunc sed. Odio ut enim blandit volutpat maecenas volutpat blandit. Egestas sed tempus urna et pharetra. Sed adipiscing diam donec adipiscing tristique risus nec feugiat in. Neque convallis a cras semper. Sed turpis tincidunt id aliquet risus feugiat. Dictum sit amet justo donec enim diam vulputate. Tellus at urna condimentum mattis pellentesque id nibh tortor id. Feugiat sed lectus vestibulum mattis ullamcorper velit. Morbi enim nunc faucibus a pellentesque sit amet porttitor eget. Nullam ac tortor vitae purus faucibus ornare. Faucibus in ornare quam viverra orci. At urna condimentum mattis pellentesque id nibh tortor. Tempor nec feugiat nisl pretium fusce. Leo a diam sollicitudin tempor. Urna id volutpat lacus laoreet non. Tortor vitae purus faucibus ornare. Nulla porttitor massa id neque aliquam vestibulum morbi blandit. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus mattis. Sit amet purus gravida quis. Commodo odio aenean sed adipiscing diam donec adipiscing tristique risus. Pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum. Posuere ac ut consequat semper viverra. Rhoncus mattis rhoncus urna neque viverra. Aenean pharetra magna ac placerat. Eleifend quam adipiscing vitae proin. Nunc sed velit dignissim sodales ut eu sem integer. Dictumst quisque sagittis purus sit amet volutpat consequat mauris nunc. Diam sit amet nisl suscipit. Arcu odio ut sem nulla. Aliquam malesuada bibendum arcu vitae elementum curabitur vitae. Sem viverra aliquet eget sit amet tellus cras adipiscing. Volutpat odio facilisis mauris sit. Diam vulputate ut pharetra sit amet. Habitant morbi tristique senectus et netus et malesuada fames ac. Fames ac turpis egestas integer eget aliquet. Condimentum vitae sapien pellentesque habitant morbi tristique senectus. Commodo viverra maecenas accumsan lacus vel facilisis volutpat est. Risus nec feugiat in fermentum posuere urna nec tincidunt. Cursus eget nunc scelerisque viverra mauris in aliquam. Lacus vel facilisis volutpat est velit. Ac tortor dignissim convallis aenean et tortor at risus viverra. Pharetra pharetra massa massa ultricies mi quis hendrerit. Facilisi cras fermentum odio eu feugiat pretium. Quis lectus nulla at volutpat diam. Cursus metus aliquam eleifend mi. Netus et malesuada fames ac. Laoreet suspendisse interdum consectetur libero id faucibus nisl. Porta non pulvinar neque laoreet suspendisse interdum consectetur. Viverra ipsum nunc aliquet bibendum enim facilisis gravida. Imperdiet sed euismod nisi porta lorem mollis. Id neque aliquam vestibulum morbi blandit cursus risus at ultrices. Ullamcorper malesuada proin libero nunc consequat. Dolor morbi non arcu risus quis. Elit duis tristique sollicitudin nibh sit. Magna etiam tempor orci eu. Rhoncus mattis rhoncus urna neque. Lorem ipsum dolor sit amet consectetur adipiscing elit. Cursus metus aliquam eleifend mi. Ut aliquam purus sit amet luctus venenatis lectus magna fringilla. Felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices. Morbi tincidunt ornare massa eget egestas purus viverra accumsan. Est ultricies integer quis auctor elit sed vulputate mi sit. Massa sed elementum tempus egestas sed. Tellus in hac habitasse platea dictumst vestibulum rhoncus. Metus vulputate eu scelerisque felis imperdiet proin. Felis bibendum ut tristique et egestas quis. Ipsum consequat nisl vel pretium.',
      permalink: 'title1',
      author: 'Admin',
      datePublished: '2017-05-04'
    }, {
      id: 2,
      title: 'Title 2',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim. Potenti nullam ac tortor vitae purus faucibus ornare suspendisse. Elementum nibh tellus molestie nunc. Tellus at urna condimentum mattis pellentesque id nibh tortor id.',
      permalink: 'title2',
      author: 'Admin',
      datePublished: '2017-06-04'
    }, {
      id: 3,
      title: 'Title 3',
      content: 'eget velit aliquet sagittis id consectetur purus ut faucibus pulvinar elementum integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam ut venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo vel orci porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor orci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui nunc mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa id neque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id ornare arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum est ultricies integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus sit amet dictum sit amet justo donec enim diam vulputate ut pharetra sit amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim sed faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu cursus euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras ornare arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim sit amet venenatis urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt augue interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis tincidunt id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus vestibulum sed arcu non odio euismod lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id interdum velit laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nisl purus in mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac auctor augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis massa sed elementum tempus egestas sed sed risus pretium quam',
      permalink: 'simple-title3',
      author: 'Admin',
      datePublished: '2017-07-04'
    }, {
      id: 4,
      title: 'Title 4',
      content: '',
      permalink: 'title4',
      author: 'Admin',
      datePublished: '2017-08-04'
    }],
    getAll: function() {
      return this.posts;
    },
    getPostById: function(id) {
      for (var i in this.posts) {
        if (this.posts[i].id == id) {
          return this.posts[i];
        }
      }
    },
  }
});